# Reverse Proxy

Este servicio de Docker, comprende la funcionalidad de proxy reverso para la comunicación entre los servicios que componen al proyecto. De esta manera nos permite escalar los servicios (hacia arriba o abajo) cuando hay necesidad sin tener que afectar la operativa del mismo.

Se definieron las siguientes locaciones:
- /ordenes/
    Refiere al servicio de ordenes
- /envios/
    Refiere al servicio de envios de donde se puede obtener, crear o modificar envios y transportes
- /libreria/
    Refiere al servicio de libreria de donde se puede obtener, crear o modificar socios, libros, generos, editoriales, autores.
- /consultas_gerenciales/
    Refiere al servicio de consultas gerenciales desde el cual se aglutinan los reportes necesarios para el entendimiento a nivel gerencial de las operaciones.
